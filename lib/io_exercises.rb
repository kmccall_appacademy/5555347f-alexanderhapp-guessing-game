# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  guess = 0
  number_of_guesses = 0
  secret_number = rand(100) + 1
  until guess == secret_number
    puts "Guess a number between 1 and 100\n"
    guess = gets.chomp.to_i
    puts "\n#{guess} is too high\n\n" if guess > secret_number
    puts "\n#{guess} is too low\n\n" if guess < secret_number
    number_of_guesses += 1
  end
  puts "\n#{guess} is correct! It only took you #{number_of_guesses} guesses!\n\n"
  "game over"
end

def file_shuffler
  puts "Enter the name of the file you'd like shuffled\n"
  file_name = gets.chomp
  lines = File.readlines(file_name)
  shuffled_lines = lines.shuffle
  new_file_name = "#{file_name.split('.')[0]}-shuffled.txt"
  File.open(new_file_name, "w") do |f|
    shuffled_lines.each { |line| f.puts line }
  end
end
